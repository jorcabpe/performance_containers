# Autor: Jorge Cabrejas Peñuelas

El objetivo de este readme es mostrar la estructura de carpetas del proyecto llevado a cabo para estudiar las prestaciones
del UnionFS. 

Podemos destacar las siguientes carpetas:
- Paper: Archivos latex junto con el paper generado (CC.pdf).
- Proyecto python donde se generan los archivos:
    - Carpeta build donde se muestran las dos aplicaciones python que se han utilizado para modificar el fichero presente
    en la imagen y poder estudiar el comportamiento del UnionFS.
    - Carpeta conf para la configuración.
    - Archivo performance_containers.ipynb es un jupyter-notebook con el código en python de los resultados de las diferentes
    secciones junto con las imágenes.

