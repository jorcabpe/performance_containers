%---------------------------------------------------------------
% PhDthesis version 1 style file by Daniel Calabuig, 2009
% based on PhDthesis version 1 style file by Daniel S�ncchez, 2008
% based on PhDthesisPSnPDF by Jakob Suckale, 2007
%---------------------------------------------------------------

\ProvidesClass{Plantillas/Template_LTE}[08/01/2009 v1 PhD thesis class]

%%Book STYLE

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions\relax
\ifx\pdfoutput\undefined
   \LoadClass[dvips, a4paper]{book}
\else
   \LoadClass[pdftex, a4paper]{book}
\fi

%:-------------------------- packages for fancy things -----------------------
\usepackage{amsmath} %For mathematics
\usepackage{graphics} % for improved inclusion of graphics
\usepackage{fancyhdr} % for better header layout
\usepackage[center]{subfigure}
\usepackage[center,labelsep=period]{caption}%%Esta l�nea debe estar activada si se desea
                    % que los pies de las figuras tengan el texto centrado. Si no est� activada aparece justificado a la izquierda
\usepackage{ifpdf}
\usepackage[nottoc]{tocbibind}%Con este package en el �ndice aparecen tambi�n la p�gina de las referencias.

%\usepackage{tabularx}
\usepackage{tabulary}

%:-------------------------- PDF/PS setup -----------------------

\newif \ifpdf
    \ifx \pdfoutput \undefined
        % for running latex
        \pdffalse
    \else
        % for running pdflatex
        \pdfoutput = 1    % positive value for a PDF output
                          % otherwise a DVI
        \pdftrue
\fi

\ifpdf
    \DeclareGraphicsExtensions{.png, .jpg, .jpeg, .pdf, .gif} %GIF doesn't work??
    %\usepackage[pdftex]{hyperref}
    %\usepackage[ pdftex, plainpages = false, pdfpagelabels,   pdfpagelayout = useoutlines,
    %             bookmarks, bookmarksopen = true,   bookmarksnumbered = true,
    %             colorlinks = true,  % was true
    %             linkcolor = black,   urlcolor  = black,   citecolor = black,     anchorcolor = black,
    %             hyperindex = black,  hyperfigures
    %             ]{hyperref}
    \usepackage[pdftex]{graphicx}
    \pdfcompresslevel=9
    \graphicspath{}
\else
    \DeclareGraphicsExtensions{.eps, .ps}
    \usepackage{epsfig}
    \usepackage{graphicx}
    \graphicspath{}
\fi


%:-------------------------- page layout -----------------------

%Tama�o exigido por la UPV

\setlength{\paperheight}{240mm}
\setlength{\paperwidth}{170mm}

%\ifpdf
  \pdfpageheight=240mm
  \pdfpagewidth=170mm
%\else
%  \setlength{\paperheight}{240mm}
%  \setlength{\paperwidth}{170mm}
%\fi

\setlength{\hoffset}{0.00cm}
\setlength{\voffset}{0.00cm}

%: Uncomment this secion for two-sided printing
% ------------------------------
\setlength{\oddsidemargin}{0cm}
\setlength{\evensidemargin}{0cm}
\setlength{\topmargin}{-1.2cm}
\setlength{\headheight}{1.36cm}
\setlength{\headsep}{0.8cm}
\setlength{\textheight}{17.34cm}
\setlength{\textwidth}{12cm}
\setlength{\marginparsep}{1mm}
\setlength{\marginparwidth}{3cm}
\setlength{\footskip}{1.5cm}


%: section below defines fancy page layout options
% ------------------------------
\pagestyle{fancy}
%\renewcommand{\chaptermark}[1]{\markboth{\MakeUppercase{\thechapter. #1 }}{}}%Sin 'Chapter' en la cabecera
\renewcommand{\chaptermark}[1]{\markboth{\MakeUppercase{\chaptername\ \thechapter. #1 }}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\fancyhf{}
\fancyhead[RO]{\bfseries\rightmark}
\fancyhead[LE]{\bfseries\leftmark}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\addtolength{\headheight}{0.5pt}
\fancypagestyle{plain}{
  \fancyhead{}
  \renewcommand{\headrulewidth}{0pt}
}


%:-------------------------- title page layout -----------------------

% starts roman page numbering until chapter 1
% important to avoid two pages numbered 1 and 2 which may cause bad links
% bug: cover i + back side ii and then numbering restarts with i; should be iii
\renewcommand{\thepage}{\roman{page}}

\newcommand{\submittedtext}{{A thesis submitted for the degree of}}

% DECLARATIONS
% These macros are used to declare arguments needed for the
% construction of the title page and other preamble.

% The year and term the degree will be officially conferred
\def\degreedate#1{\gdef\@degreedate{#1}}
% The full (unabbreviated) name of the degree
\def\degree#1{\gdef\@degree{#1}}
% The name of your college or department(eg. Trinity, Pembroke, Maths, Physics)
\def\collegeordept#1{\gdef\@collegeordept{#1}}
% The name of your University
\def\university#1{\gdef\@university{#1}}
% Defining the crest
\def\crest#1{\gdef\@crest{#1}}
% Stating the city of birth for title page where needed; uncommented for use
%\def\cityofbirth#1{\gdef\@cityofbirth{#1}}

% These macros define an environment for front matter that is always
% single column even in a double-column document.

\newenvironment{alwayssingle}{%
       \@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
       \else\newpage\fi}
       {\if@restonecol\twocolumn\else\newpage\fi}

%define title page layout
\renewcommand{\maketitle}{%
\begin{alwayssingle}
    \renewcommand{\footnotesize}{\small}
    \renewcommand{\footnoterule}{\relax}
    \thispagestyle{empty}
%  \null\vfill
  \begin{center}
    {{{\@crest} \par} \vspace*{30mm}}
    { \Huge {\bfseries {\@title}} \vspace*{30mm} \par}
    {{\Large \@author} \par}
%	\vspace*{1ex}
%	{{\@cityofbirth} \par}
{\large
	\vspace*{1ex}
    {{\@collegeordept} \par}
	\vspace*{1ex}
    {{\@university} \par}
	\vspace*{25mm}
    {{\submittedtext} \par}
	\vspace*{1ex}
    {\it {\@degree} \par}
	\vspace*{2ex}
    {\@degreedate}
}%end large
  \end{center}
  \null\vfill
\end{alwayssingle}}

% page number for cover back side should have page number blanked

%:-------------------------- front matter layout -----------------------
%%%TABLE OF CONTENTS (change of name to "Table of contents")
\renewcommand{\contentsname}{Table of contents} %Unactivate for just "constents"
% DEDICATION
%
% The dedication environment makes sure the dedication gets its
% own page and is set out in verse format.

\newenvironment{dedication}
{\begin{alwayssingle}
  \pagestyle{empty}
  \begin{center}
  \vspace*{1.5cm}
  {\LARGE }
  \end{center}
  \vspace{0.5cm}
  \begin{quote} \begin{center}}
{\end{center} \end{quote} \end{alwayssingle}}


% ACKNOWLEDGEMENTS
%
% The acknowledgements environment puts a large, bold, centered
% "Acknowledgements" label at the top of the page. The acknowledgements
% themselves appear in a quote environment, i.e. tabbed in at both sides, and
% on its own page.

\newenvironment{acknowledgements}
{\pagestyle{empty}
\begin{alwayssingle}
\begin{center}
\vspace*{0.5cm}
{\Large \bfseries Acknowledgements}
\end{center}
\vspace{0.5cm}
\begin{quote}}
{\end{quote}\end{alwayssingle}}

% The acknowledgementslong environment puts a large, bold, centered
% "Acknowledgements" label at the top of the page. The acknowledgement itself
% does not appears in a quote environment so you can get more in.

\newenvironment{acknowledgementslong}
{\pagestyle{empty}
\begin{alwayssingle}
\begin{center}
\vspace*{0.5cm}
{\LARGE \bfseries Acknowledgements}
\end{center}
\vspace{0.5cm}\begin{quote}}
{\end{quote}\end{alwayssingle}}

%ABSTRACT
%
%The abstract environment puts a large, bold, centered "Abstract" label at
%the top of the page. The abstract itself appears in a quote environment,
%i.e. tabbed in at both sides, and on its own page.

\newenvironment{abstracts} {\begin{alwayssingle} \pagestyle{empty}
  \begin{center}
  \vspace*{0.5cm}
  {\Large \bfseries  Abstract}
  \end{center}
  \vspace{0.5cm}
   \begin{quote}}
{\end{quote}\end{alwayssingle}}

%The abstractlong environment puts a large, bold, centered "Abstract" label at
%the top of the page. The abstract itself does not appears in a quote
%environment so you can get more in.

\newenvironment{abstractslong} {\begin{alwayssingle} \pagestyle{empty}
  \begin{center}
  \vspace*{0.5cm}
  {\LARGE \bfseries  Abstract}
  \end{center}
  \vspace{0.5cm} \begin{quote}}
{\end{quote}\end{alwayssingle}}

%The abstractseparate environment is for running of a page with the abstract
%on including title and author etc as required to be handed in separately

\newenvironment{abstractseparate} {\begin{alwayssingle} \pagestyle{empty}
  \vspace*{-1in}
 \begin{center}
    { \Large {\bfseries {\@title}} \par}
    {{\large \vspace*{1ex} \@author} \par}
{\large \vspace*{1ex}
    {{\@collegeordept} \par}
    {{\@university} \par}
\vspace*{1ex}
    {{\it \submittedtext} \par}
    {\it {\@degree} \par}
\vspace*{2ex}
    {\@degreedate}}
  \end{center}}
{\end{alwayssingle}}

%Statement of originality if required

\newenvironment{declaration} {\begin{alwayssingle} \pagestyle{empty}
  \begin{center}
  \vspace*{1.5cm}
  {\Large \bfseries  Declaration}
  \end{center}
  \vspace{0.5cm}
   \begin{quote}}
{\end{quote}\end{alwayssingle}}


%:-------------------------- page numbers: roman+arabic -----------------------

% ROMANPAGES
%
% The romanpages environment set the page numbering to lowercase roman one
% for the contents and figures lists. It also resets
% page-numbering for the remainder of the dissertation (arabic, starting at 1).

\newenvironment{romanpages}
%{
%	\setcounter{page}{1}
%	\renewcommand{\thepage}{\roman{page}}
%} % close romanpage env't

{\newpage\renewcommand{\thepage}{\arabic{page}}\setcounter{page}{1}}
