\documentclass[10pt, conference, letterpaper]{IEEEtran}

\usepackage[nolist]{acronym}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{listings}
\usepackage[obeyspaces]{url}

\hyphenation{va-ria-bles}


\begin{document}
	
\input{acronyms}

\title{Análisis de Prestaciones de los Contenedores Docker y la Influencia de los distintos tipos de \emph{Storage Drivers}}

\author{\IEEEauthorblockN{Jorge Cabrejas Peñuelas}
\IEEEauthorblockA{Universitat Politècnica de València\\
Email: jorcabpe@upv.es}}

\maketitle

\begin{abstract}
Las aplicaciones que hacen uso de imágenes Docker, pueden sufrir una degradación de prestaciones, sobre todo en experiencia de usuario, si no se presta atención a diferentes variables. Este documento se centra en estas variables y en cómo influyen en el tiempo de ejecución y en el espacio ocupado en disco.
En este documento, presentamos un estudio multiparamétrico  en las prestaciones de una aplicación, permitiéndonos comprender esos parámetros en un entorno controlado. Nuestro estudios reflejan diferencias en las conclusiones extraídas de la literatura.
\end{abstract}


\IEEEpeerreviewmaketitle



\section{Introduction}
% no \IEEEPARstart
En la actualidad, en la mayoría de entornos de desarrollo
de aplicaciones, es muy común el uso de imágenes Docker, vistas
como una unidad formada por un código, todas sus dependencias correspondientes, 
herramientas del sistema y una configuración~\cite{DockerWebPage}. Esta es
una gran ventaja frente al uso de máquinas virtuales, ya que la información
necesaria es más reducida y puede ser altamente portable. Esta información 
se estructura en capas de sólo lectura (ficheros
y directorios), donde cada capa representa un incremento de información
respecto a la capa anterior. 

El concepto de contenedor corresponde a una imagen en tiempo de ejecución. 
Su estructura interna puede ser vista como una capa de lectura y escritura
que se asienta encima del conjunto de capas de la imagen (ver Figura~\ref{docker}). Esta estructura 
completa necesita ser montada y servida como un sistema de ficheros de la 
instancia del contenedor a través de una union de directorios (\emph{overlay}) 
bajo el sistema de ficheros del \emph{host}, conocido como
el \emph{Backing File System} (ejemplo, Linux Ext3/Ext4). 

\begin{figure*} [t]	
	\centering
	\includegraphics[width=\textwidth, scale=0.9]{Figuras/PDF/docker.pdf}
	\caption{Arquitectura multicapa de Docker.}
	\label{docker}
\end{figure*}

En un principio, cuando se levanta un contenedor, la capa de lectura y escritura 
está vacía y es cuando se modifica 
algún fichero por primera vez, cuando el \emph{Storage Driver} copia el elemento modificado 
a esta capa. Todas las posteriores modificaciones del fichero se harán sobre esta capa, sin
necesidad de realizar ninguna acción sobre versiones anteriores del fichero.
Este mecanismo, conocido en la literatura como \emph{Copy
on Write (CoW)}, impone un \emph{overhead} 
importante ya que el fichero, aunque se modifique una pequeña parte del mismo, 
debe ser copiado primero, pudiendo provocar un alto coste temporal y provocando que el 
tamaño de la capa aumente significativamente~\cite{BAOVERLAY}. Por su parte, cuando se produce una 
lectura de un fichero, el sistema empieza a buscar el fichero desde la capa más alta a la más baja. 
Una vez encontrado el fichero, la información del \emph{path} es \emph{cacheada}
en el sistema para futuros accesos.

Para ilustrar el \emph{overhead} incurrido en una arquitectura multicapa, comparamos la latencia
o el retardo de una operación de lectura y escritura (\emph{append}). En concreto, ejecutamos una 
simple aplicación \emph{dockerizada} que accede a un fichero y lo modifica. Se ha probado además la 
diferencia en prestaciones de ubicar este fichero dentro del contenedor y por tanto llevar a cabo
una operación de CoW o usar un volumen sin el coste asociado a esa misma operación (non-CoW).
Nuestras investigaciones demuestran que las operaciones CoW incurren en un retardo considerable frente al uso
de volúmenes. Por tanto, es una buena alternativa a los sistema \emph{overlay},
a costa de incurrir en un esfuerzo de portabilidad y de alguna manera a reducir
las ventajas de los contenedores Docker. El siguiente estudio realizado es el impacto que tiene en las prestaciones el uso de un número alto de capas en la imagen y cómo afecta el hecho de modificar
el fichero dependiendo de la ubicación del mismo en la estructura de capas. Nuestras investigaciones no aprecian una diferencia significativa al usar un mayor o menor número de capas. Estos resultados, sin embargo, están consonancia con algunos autores como \cite{Tarasov}. Por otra parte, también se ha estudiado si existe algún método de acceso a fichero que tenga mejores o peores prestaciones. Efectivamente, nuestros estudios detectan que las operaciones de lectura y escritura presentan una importante degradación frente a la escritura únicamente. Finalmente, comparamos el efecto de utilizar diferentes \emph{Storage Drivers} en las prestaciones del CoW tanto en retardo como en espacio ocupado. A diferencia de lo que recomienda la documentación de Docker, pero en consonancia con algunos autores~\cite{BAOVERLAY}, encontramos que los sistemas tradicionales incurren en un retardo más reducido a costa de un mayor espacio ocupado.

\section{Docker}

Docker es una tecnología de contenedores de software libre para construir aplicaciones. El sistema Docker Engine es el
sistema que actúa como una aplicación cliente/servidor formado por el demonio Docker (dockerd) que actúa como servidor y
el Command Line Interface (CLI) (docker) que actúa como cliente y que permite ejecutar comandos para construir, levantar, parar o borrar contenedores o imágenes~\cite{arquitectura}. El demonio Docker y cliente Docker se comunican a través de un REST API.

\subsection{Imágenes Docker}
Los sistemas de software modernos se basan en el concepto de sistema de ficheros donde se almacenan binarios, configuraciones, datos e información de sistema (por ejemplo, el directorio /sys). Precísamente, Docker se basa en este sistema de ficheros, pero los estructura en capas apiladas, de tal manera que, los contenedores ven los ficheros modificados y no modificados de las capas inferiores. Sin embargo, si dos capas inferiores contienen el mismo fichero, el contenedor únicamente se quedará con el fichero de la capa más cercana a él.

Las capas se identifican mediante un hash y se pueden compartir entre diferentes imágenes. Por ejemplo, a menudo la primera capa (la inferior) representa una distribución de Linux y ésta es compartida por diferentes imágenes del sistema local.

Esiste un almacen de imágenes Docker público, conocido como Docker Hub, formado por más de cuatro cientas mil imágenes. Los usuarios pueden compartir estas imágenes y crearse nuevas a partir de ellas. Por supuesto, también se pueden tener almacenes de imágenes privados para ser utilizados.

\subsection{Storage Drivers}
En la introducción ya se avanzó el concepto de \emph{Storage Driver} que consiste en un sistema que gestiona las diferentes capas y cómo éstas son almacenadas~\cite{storage}. Por tanto, el \emph{Storage Driver} es responsable de preparar el sistema de ficheros para un contenedor. Note que este elemento es clave a la hora de elegir la manera de persistir los datos de las aplicaciones y como luego se verá en los resultados, podría ser limitante en producción.

A continuación, explicamos algunos tipos de \emph{Storage Driver}.

\subsubsection{VFS}
El \emph{Storage Driver} VFS no es un sistema de ficheros como el descrito en la Sección~I. En este sistema, cada capa es un directorio en disco y no se basa en el método CoW~\cite{vfs}. Para crear una nueva capa, se copia la capa anterior. En la documentación de Docker se nombra que este sistema ofrece peores prestaciones así como un mayor uso de disco. Además, se destaca que no es un buen sistema para producción y que únicamente debería ser utilizado como referencia a otros sistemas para compararlos en prestaciones. De nuestros resultado, sin embargo, ofrece retardos reducidos a costa de incrementar el espacio en disco.

\subsubsection{AUFS}
AUFS, a diferencia de VFS, sí es un sistema como el descrito en la Sección~I~\cite{aufs}. La información de las capas de las imágenes y del contenedor es almacenada en \emph{/var/lib/docker/aufs}. Las prestaciones de este sistema depende de los tipos de acceso a los ficheros, al número capas y a su distribución entre ellas. Nuestras pruebas ilustran que AUFS podría ser un buen sistema dado que ofrece un retardo reducido sin un coste de almacenamiento elevado.

\subsubsection{Overlay y Overlay2}
Overlay es una versión anterior a Overlay2 y sólo soportaba dos capas, una primera capa inferior de sólo lectura y una capa superior mutable. Overlay2 soporta 128 capas inferiores sin necesidad de enlaces como ocurre en el sistema Overlay. Ambos sistemas son similares a AUFS, pero en la documentación se argumenta que son más rápidos y que tienen una implementación más simple que AUFS~\cite{overlay_overlay2}. Nuestras investigaciones, sin embargo, muestran que AUFS tiene mejores prestaciones. Uno de los principales motivos de estas diferencias podrían deberse a que Overlay y Overlay2 utilizan \emph{page caching} donde múltiples contenedores comparten información para el mismo fichero. Sin embargo, en los estudios realizados no se han probado diferentes contenedores a la vez y de ahí la diferencia.

Existen otros \emph{Storage Drivers} como el device-mapper~\cite{devicemapper}, Btrfs~\cite{btrfs} o ZFS~\cite{zfs} que tienen características diferentes a los mostrados hasta ahora. Por ejemplo, todos ellos trabajan a nivel de bloque en lugar de a nivel de fichero. Por tanto, si se produce una modificación de alguna parte del fichero, no se copia todo el fichero como ocurre en AUFS u Overlay2, sino que se copia únicamente el bloque correspondiente.

Como resumen, existen diferentes \emph{drivers} que cada uno podría ser más adecuado en ciertas circunstancias. Por ejemplo, si un fichero grande es actualizado en un contenedor, AUFS y Overlay2 copiarán el fichero entero en la capa de lectura y escritura con la consiguiente degradación de prestaciones. En otra situación donde haya muchos ficheros y con directorios profundos, AUFS buscará en cada uno de esos directorios incrementando el retardo de las aplicaciones.

\subsection{Capas de las imágenes}
Otra de las variables a considerar es el número de capas que tiene la imagen. En la literatura, se recomienda reducir ese número a lo mínimo posible~\cite{bestpractices}. En este documento se estudian las prestaciones en función del número de capas que disponga la imagen.


\section{Resultados y discusión}

A lo largo de la sección anterior, se han explicado algunas de las variables o dimensiones que afectan a las prestaciones generales del uso de contenedores Docker. Existen otras variables como el tipo de disco que sustenta Docker o el tipo de sistema de ficheros utilizado del \emph{host} que podrían influir en las prestaciones. En esta sección, intentamos mostrar de manera empírica el impacto en las prestaciones de algunas variables definidas en la sección anterior.

\subsection{Configuración}

En nuestros experimentos utilizamos un servidor Intel Core i5-8250U CPU~@~1.6~GHz con 8~GB de memoria RAM y un sistema operativo Windows de 64~bits. Este sistema operativo contiene una máquina virtual bajo el software Oracle VM Virtual Box. La máquina virtual tiene una distribución Ubuntu~20.04 con 5.185~GB de memoria RAM y 4 procesadores. La versión del kernel es 5.4.0-58-generic. Las pruebas llevadas a cabo se han realizado sobre la máquina virtual Linux. El servidor contiene un disco SSD suficiente para Docker. La versión de Docker es la 19.03.13. Finalmente, utilizamos como sistema de ficheros de la máquina virtual el sistema Ext4.

Se han realizado 100 simulaciones por cada estudio que se va a presentar y se ha calculado la mediana de estos 100 valores para eliminar posibles valores anómalos y tener en cuenta la varianza estadística de las simulaciones. Por último decir que si no se dice lo contrario el \emph{Storage Driver} utilizado será el Overlay2, que es el sistema recomendado por la documentación de Docker.

\subsection{Influencia del tamaño del fichero en las prestaciones}
Como se ha dicho anteriormente, el storage driver Overlay2 utiliza el método CoW cuando el contenedor modifica un fichero contenido en las capas de la imagen. 

A continuación, se muestra un ejemplo de Dockerfile que se construye a partir de una imagen de Ubuntu. El Dockerfile crea un archivo llamado file.txt que será modificado mediante una operación de \emph{append} por el fichero python read\_write.py. El número total de capas de la imagen construida es de cuatro.

\begin{lstlisting}
FROM ubuntu
RUN apt-get update && \
	apt-get install -y python3 && \
	truncate -s 1MB file.txt
COPY read_write.py /
ENTRYPOINT ["tail", "-f", "/dev/null"]
\end{lstlisting}

Se quiere demostrar que el hecho de modificar el fichero \emph{file.txt} tiene un coste temporal (retardo) y que además depende del tamaño del mismo. La Figura~\ref{fig1} muestra ese coste temporal cuando se realiza una operación de lectura y escritura a la vez. Se observa que se tiene un retardo entre 9~ms a 1~s para ficheros entre 1MB y 1GB.

\begin{figure} [h]	
	\centering
	\includegraphics[width=\columnwidth]{Figuras/PDF/fig1.pdf}
	\caption{Influencia del tamaño de fichero en las prestaciones del sistema Overlay2}
	\label{fig1}
\end{figure}

\subsection{Comparación de prestaciones al usar la capa de lectura/escritura frente a usar volúmenes}
Los volúmenes son a menudo utilizados como almacenes de datos externos al contenedor. Este volumen no es más que un directorio del \emph{host} donde se levanta el contenedor. Los contenedores suelen leer de estos volúmenes, pero también pueden escribir en ellos si así se indica.

En este caso, el Dockerfile no construirá el fichero para añadir información como en el caso anterior:
\\
\\
\begin{lstlisting}
FROM ubuntu
RUN apt-get update && \
	apt-get install -y python3
COPY read_write.py /
ENTRYPOINT ["tail", "-f", "/dev/null"],
\end{lstlisting}
sino que modificará el fichero que se encuentra en una ruta del \emph{host} a través de la opción \emph{-v} al crear el contenedor.
 
Como se muestra en la Figura~\ref{fig2} existe una importante diferencia entre usar el volumen o no. De hecho, como es lógico, el retardo sufrido al modificar el fichero \emph{file.txt} en el caso de tener un volumen, es independiente con el tamaño del fichero; su variación es mínima y sólo se debe a la varianza estadística de las simulaciones. Sin embargo, al usar la capa de lectura y escritura (r/w) del contenedor, la degradación es evidente a medida que aumenta el tamaño del fichero. La mejoría de prestaciones puede alcanzar el ratio entre un valor de 15 a 2000. Por tanto, parece que el uso de volúmenes es clave a la hora de mejorar el rendimiento de las aplicaciones.

\begin{figure} [h]	
	\centering
	\includegraphics[width=\columnwidth]{Figuras/PDF/fig2.pdf}
	\caption{Comparación de prestaciones entre el uso de la capa de lectura/escritura del contenedor y un volumen.}
	\label{fig2}
\end{figure}

\subsection{Influencia del número de capas en las prestaciones del sistema}
En la literatura, existen varios autores que sostienen que los sistemas que usan la capa de lectura y escritura, se degradan con el número de capas~\cite{bestpractices}~\cite{Harter}. Sin embargo, existen otros autores como \cite{Tarasov} cuyas conclusiones son parecidas a nuestras investigaciones.

Se han probado añadir 100 capas al Dockerfile, donde el fichero a modificar se encuentra en las capas superiores:

\begin{lstlisting}
FROM ubuntu
RUN apt-get update && \
	apt-get install -y python3 && \
	truncate -s 1MB file.txt
¡¡¡¡100 CAPAS UBICADAS AQUÍ!!!!
COPY read_write.py /
ENTRYPOINT ["tail", "-f", "/dev/null"]
\end{lstlisting}

o en las capas inferiores:
\begin{lstlisting}
FROM ubuntu
RUN apt-get update && \
	apt-get install -y python3
¡¡¡¡99 CAPAS UBICADAS AQUÍ!!!!
COPY read_write.py /
truncate -s 1MB file.txt
ENTRYPOINT ["tail", "-f", "/dev/null"]
\end{lstlisting}

Se quería estudiar si existía algún patrón especial al ubicar el fichero en lo más alto (en la capa 2) o lo más bajo del sistema de capas (en la capa 103). Se han comparado además los resultados con el caso visto en la Sección~II-B.

\begin{figure} [h]	
	\centering
	\includegraphics[width=\columnwidth]{Figuras/PDF/fig3.pdf}
	\caption{Comparación de prestaciones al usar la capa de lectura/escritura a medida que aumentamos el número de capas.}
	\label{fig3}
\end{figure}

Como muestra la Figura~\ref{fig3}, nuestros resultados indican que usar más o menos capas en el sistema Overlay2 no afecta a las prestaciones de la lectura y escritura. Se entiende que la variación que existe se debe a la propia variabilidad de las simulaciones y sus condiciones de carga del sistema.

\subsection{Influencia del método de acceso}
El método de acceso al fichero también podría afectar a las prestaciones de las arquitecturas multicapa. En este caso se ha probado además el acceso a escritura y se ha comparado con el método que se ha utilizado en los anteriores apartados, el método de lectura y escritura (\emph{append}).

La Figura~\ref{fig4} muestra la comparación de prestaciones de dos métodos de acceso estudiados. Se observa que el método de acceso de escritura es independiente del tamaño de fichero. Esto hace pensar que este tipo de acceso no provoca que el fichero se lo copie en la capa de lectura y escritura, sino que aunque se llamen igual, el fichero se crea de nuevo.

\begin{figure} [h]	
	\centering
	\includegraphics[width=\columnwidth]{Figuras/PDF/fig4.pdf}
	\caption{Comparación de prestaciones al usar diferentes tipos de acceso.}
	\label{fig4}
\end{figure}

\subsection{Influencia del storage driver en las prestaciones}

En esta sección evaluamos diferentes \emph{Storage Drivers}. En concreto, evaluamos VFS, AUFS, Overlay y Overlay2. Todos ellos soportados en la versión de Ubuntu~20.04. Para cambiar de \emph{Storage Driver} únicamente hay que parar el servicio Docker, recargar la configuración del demonio de Docker (\emph{systemctl daemon-reload}), incluir en el servicio que llama al demonio de Docker \emph{-{}-storage-driver} y reiniciar el servicio.

En la Figura~\ref{fig5} se muestra que el retardo de Overlay y Overlay2 es peor que el retardo de AUFS, con un ratio entre 3 y 6 con un tamaño de fichero entre 1MB y 1GB. Por su parte, VFS tiene un retardo constante de unos 400~microsegundos, bastante por debajo del retardo de los sistemas que utilizan CoW.

\begin{figure} [h]	
	\centering
	\includegraphics[width=\columnwidth]{Figuras/PDF/fig5.pdf}
	\caption{Influencia del storage driver en el retardo.}
	\label{fig5}
\end{figure}

Uno podría pensar en utilizar VFS como \emph{Storage Driver}, sin embargo, VFS es el sistema que ocupa más espacio en su directorio raíz de Docker (\path{\var\lib\docker\<storage-driver>}). La diferencia de tamaño puede llegar a ser importante, de 3 a 4 veces más que el
sistema AUFS. Por tanto, evaluando las Figuras~\ref{fig5} y \ref{fig6}, AUFS podría llegar a considerarse un sistema de compromiso entre prestaciones y espacio que ocupa en disco.

\begin{figure} [h]	
	\centering
	\includegraphics[width=\columnwidth]{Figuras/PDF/fig6.pdf}
	\caption{Influencia del storage driver en el tamaño del directorio raíz de Docker.}
	\label{fig6}
\end{figure}


\section{Conclusión}
La conclusión de este paper es que es necesario un estudio previo de las diferentes variables que influyen en las prestaciones de una aplicación. De nuestras investigaciones concluimos que a pesar de que en la documentación se establece una serie de pautas o consejos a llevar a cabo en la aplicación \emph{dockerizada}, estas pautas deberían ser revisadas. Por ejemplo, nosostros encontramos que el sistema AUFS ofrece en general mejores prestaciones (retardo y espacio ocupado) que el sistema recomendado Overlay2 para nuestra aplicación. Además, no encontramos una diferencia significativa al usar un mayor o menor número de capas en un sistema Overlay2. Finalmente, sí encontramos una gran mejoría con el hecho de utilizar volúmenes en los contenedores, en contraposición a usar la capa de lectura y escritura.

\begin{thebibliography}{1}
	
\bibitem{DockerWebPage}
https://www.docker.com/resources/what\-container. Accedido 19-12-2020.

\bibitem{BAOVERLAY}
Yu~Sun, Jiaxin~Lei, Seunghee~Shin, and Hui Lu \emph{BAOVERLAY: A Block-Accessible Overlay File System for Fast and Efficient Container Storage}, ACM Symposium on Cloud Computing (SoCC ’20), October 19–21, 2020, Virtual Event, USA.

\bibitem{Tarasov}
Vasily~Tarasov, Lukas~Rupprecht, Dimitris~Skourtis, Wenji~Li, Raju~Rangaswami and Ming~Zhao \emph{Evaluating Docker storage performance: from workloads to graph drivers}, Cluster Computing (2019) 22:1159–1172.

\bibitem{arquitectura}
https://docs.docker.com/get-started/overview/\#docker\-architecture. Accedido el 19-12-2020.

\bibitem{storage}
https://docs.docker.com/storage/storagedriver/. Accedido el 19-12-2020.

\bibitem{vfs}
https://docs.docker.com/storage/storagedriver/vfs\-driver/. Accedido el 19-12-2020.

\bibitem{aufs}
https://docs.docker.com/storage/storagedriver/aufs-driver/. Accedido el 19-12-2020.

\bibitem{overlay_overlay2}
https://docs.docker.com/storage/storagedriver/overlayfs\-driver/. Accedido el 19-12-2020.

\bibitem{devicemapper}
https://docs.docker.com/storage/storagedriver/device-mapper\-driver/. Accedido el 19-12-2020.

\bibitem{btrfs}
https://docs.docker.com/storage/storagedriver/btrfs\-driver/. Accedido el 19-12-2020.

\bibitem{zfs}
https://docs.docker.com/storage/storagedriver/zfs\-driver/. Accedido el 19-12-2020.

\bibitem{bestpractices}
https://docs.docker.com/develop/develop\-images/dockerfile\_best-practices/. Accedido el 19-12-2020.

\bibitem{Harter}
Tyler~Harter, Brandon~Salmon, Rose~Liu, Andrea C. Arpaci-Dusseau, and Remzi H. Arparci-Dusseau \emph{Slacker: Fast Distribution with Lazy Docker Containers}. Proceedings of the 14th USENIX Conference on File and Storage Technologies (FAST) (2016).


\end{thebibliography}

\end{document}


